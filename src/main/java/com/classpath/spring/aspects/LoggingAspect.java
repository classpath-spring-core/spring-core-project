package com.classpath.spring.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Aspect
@Component
//@EnableAspectJAutoProxy
public class LoggingAspect {

    @Before("within(com.classpath.spring.service..*)")
    public void logBefore(JoinPoint method){
        Object[] args = method.getArgs();
        Signature signature = method.getSignature();

        System.out.println(" Logging before the execution of "+signature.getName() + " with args "+ args.length);
    }
    @After("within(com.classpath.spring.service..*)")
    public void logAfter(JoinPoint method) {
        Object[] args = method.getArgs();
        Signature signature = method.getSignature();
        System.out.println(" Logging after the execution of "+signature.getName() + " with args "+ args.length);
    }

}