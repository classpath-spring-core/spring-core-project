package com.classpath.spring.model;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = "productId")
public class Product {

    private long productId;

    private String name;

    private double price;

}