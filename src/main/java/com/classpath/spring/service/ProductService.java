package com.classpath.spring.service;

import com.classpath.spring.exception.ProductNotFoundException;
import com.classpath.spring.model.Product;
import java.util.Set;

public interface ProductService {

    Product save(Product product);

    Set<Product> fetchAll();

    Product update(long id, Product product);

    Product findById(long id) throws ProductNotFoundException;

    void deleteById(long id);
}