package com.classpath.spring.service;

import com.classpath.spring.dao.ProductDAO;
import com.classpath.spring.exception.ProductNotFoundException;
import com.classpath.spring.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private ProductDAO productDAO;

  /*  public ProductServiceImpl(@Qualifier("productDAOImpl") ProductDAO productDAO){
        this.productDAO = productDAO;
    }
   */
    private static ProductNotFoundException throwProductNotFoundException() {
        return new ProductNotFoundException(" Product with the given Id does not exists");
    }

    @Override
    public Product save(Product product) {
        return this.productDAO.save(product);
    }

    @Override
    public Set<Product> fetchAll() {
        return this.productDAO.fetchAll();
    }

    @Override
    public Product update(long id, Product product) {
        return this.productDAO.update(id, product);
    }

    @Override
    public Product findById(long id) throws ProductNotFoundException {
        return this.productDAO.findById(id).orElseThrow(ProductServiceImpl:: throwProductNotFoundException);
    }

    @Override
    public void deleteById(long id) {
        this.productDAO.deleteById(id);
    }
}