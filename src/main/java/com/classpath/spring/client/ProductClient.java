package com.classpath.spring.client;

import com.classpath.spring.config.ApplicationConfiguration;
import com.classpath.spring.controller.ProductController;
import com.classpath.spring.model.Product;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ProductClient {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        //ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        ProductController productController = applicationContext.getBean("productController", ProductController.class);
        Product macBookPro = Product.builder().name("macBook").price(2_00_000).productId(2000L).build();
        productController.createProduct(macBookPro);
        ((AbstractApplicationContext) applicationContext).registerShutdownHook();
    }
}