package com.classpath.spring.config;

import com.classpath.spring.dao.ProductDAO;
import com.classpath.spring.dao.ProductDAOImpl;
import com.classpath.spring.model.Product;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;
import java.util.HashSet;

@Configuration
@ComponentScan("com.classpath.spring")
public class ApplicationConfiguration {

    @Bean
    @Scope
    public ProductDAO productDAO(){
        ProductDAOImpl productDAO = new ProductDAOImpl();
        productDAO.setProducts(new HashSet<>(Arrays
                .asList(
                        Product.builder().name("MacBook-Pro").price(19999).productId(1000).build(),
                        Product.builder().name("IPhone").price(9999).productId(2000).build()
                )
        ));
        return productDAO;
    }
}