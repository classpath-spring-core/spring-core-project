package com.classpath.spring.di.client;

import com.classpath.spring.di.PaymentGateway;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PaymentGatewayClient {
    public static void main(String[] args) {
        // Load the configuration and initialize the dependencies
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        PaymentGateway gateway =  applicationContext.getBean("googlePay", PaymentGateway.class);

        gateway.transfer("From", "Nikhil", 4500, "Tranfered the amount");

    }
}