package com.classpath.spring.di;

public interface PaymentGateway {

    public void transfer(String from, String to, double amount, String notes);
}