package com.classpath.spring.di;

public class AmazonPay implements PaymentGateway{

    public void transfer(String from, String to, double amount, String notes) {
        System.out.println("Amount of "+ amount + " has been transfered to "+ to + " from "+ from+ " Using Amazon Pay");
    }
}