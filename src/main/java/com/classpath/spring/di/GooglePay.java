package com.classpath.spring.di;

public class GooglePay implements  PaymentGateway{
    public void transfer(String from, String to, double amount, String notes) {
        System.out.println("Amount of "+ amount +" has been transferred from "+ from+" to: "+ to);
    }
}