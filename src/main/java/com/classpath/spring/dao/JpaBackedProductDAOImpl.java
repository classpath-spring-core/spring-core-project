package com.classpath.spring.dao;

import com.classpath.spring.model.Product;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public class JpaBackedProductDAOImpl implements ProductDAO {

    @Override
    public Product save(Product product) {
        return null;
    }

    @Override
    public Set<Product> fetchAll() {
        return null;
    }

    @Override
    public Product update(long id, Product product) {
        return null;
    }

    @Override
    public Optional<Product> findById(long id) {
        return Optional.empty();
    }

    @Override
    public void deleteById(long id) {

    }
}