package com.classpath.spring.dao;

import com.classpath.spring.model.Product;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
@Primary
public class ProductDAOImpl implements ProductDAO {


    private String dbHost;
    private String username;
    private String password;

    private Set<Product> products;


    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @PostConstruct
    public void init(){
        System.out.println(" In the initialization phase");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("In the destruction phase");
    }

    public String getDbHost() {
        return dbHost;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Product save(Product product) {
        System.out.println(" Came inside the product DAO save method ....");
        return null;
    }

    @Override
    public Set<Product> fetchAll() {
        return products;
    }

    @Override
    public Product update(long id, Product product) {
        return null;
    }

    @Override
    public Optional<Product> findById(long id) {
        return Optional.empty();
    }

    @Override
    public void deleteById(long id) {

    }
}