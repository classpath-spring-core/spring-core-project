package com.classpath.spring.dao;

import com.classpath.spring.model.Product;
import java.util.Optional;
import java.util.Set;

public interface ProductDAO {

    Product save(Product product);

    Set<Product> fetchAll();

    Product update(long id, Product product);

    Optional<Product> findById(long id);

    void deleteById(long id);

}