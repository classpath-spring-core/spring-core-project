package com.classpath.spring.controller;

import com.classpath.spring.exception.ProductNotFoundException;
import com.classpath.spring.model.Product;
import com.classpath.spring.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Set;

@Controller
@AllArgsConstructor
public class ProductController {

    private ProductService productService;

    public Product createProduct(Product product){
        return this.productService.save(product);
    }

    public Set<Product> fetchAll(){
        return null;
    }

    public Product update(long id, Product product){
        return  null;
    }

    public Product findById(long id) throws ProductNotFoundException{
        return null;
    }

    public void deleteById(long id){

    }

}